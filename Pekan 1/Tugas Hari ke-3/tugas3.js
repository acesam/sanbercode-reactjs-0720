// Soal 1 concate
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";
var upper = kataKeempat.toUpperCase();	

console.log(kataPertama.concat(kataKedua, kataKetiga, upper));

// Soal 2 change to string
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";
console.log(kataPertama.toString()); 
console.log(kataKedua.toString());
console.log(kataKetiga.toString());
console.log(kataKeempat.toString());

// Soal 3 substring

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// Soal 4 if else
var nilai = "88";
if (nilai >= 80){
	console.log("indeks A")
} else if ( 70 >= nilai < 80){ 
	console.log("indeks B")
} else if ( 60 >= nilai < 70){ 
	console.log("indeks C")
}else if ( 50 >= nilai < 60){ 
	console.log("indeks D")
}else if ( nilai < 50){ 
	console.log("indeks E")
}

// Soal 5
var tanggal = 22;
var bulan = 7;
var tahun = 1998;

console.log(tanggal)
switch(bulan) {
  case 1:   { console.log('Jan'); break; }
  case 2:   { console.log('FEb'); break; }
  case 3:   { console.log('Mar'); break; }
  case 4:   { console.log('Apr'); break; }
  case 5:   { console.log('Mei'); break; }
  case 6:   { console.log('Jun'); break; }
  case 7:   { console.log('Juli'); break; }
  case 8:   { console.log('Agus'); break; }
  case 9:   { console.log('Sept'); break; }
  case 10:   { console.log('Okt'); break; }
  case 11:   { console.log('Nov'); break; }
  case 12:   { console.log('Des'); break; }
default:  { console.log('Salah bulan'); }}
console.log(tahun)

