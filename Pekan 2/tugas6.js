// SOAL1
var arrayDaftarPeserta = {
	nama : "Asep",
	jenisKelamin : "laki-laki",
	hobi : "baca buku",
	tahun lahir : 1992,
}

console.log(arrayDaftarPeserta.nama);


// SOAL 2
var buah = [{nama: "strawberry", warna: "merah", ada bijinya: "tidak", harga: 9000}, {nama: "jeruk", warna:" "oranye", ada bijinya: "ada", harga: 8000}, {nama: "semangka", warna:" "hijau & merah", ada bijinya: "ada", harga: 10000}, {nama: "pisang", warna:" "kuning", ada bijinya: "tidak", harga: 5000}]

console.log(buah[0]);


//soal 3
var dataFilm = [{nama: "Titanic", durasi: 3, genre: "romance", tahun: 1999}, {nama: "Avengers", durasi: 2, genre: "Sci-fi", tahun: 2007}]

var arrayjudul = dataFilm.map(function(item){
   return item.nama
})
console.debug(arrayjudul)

var arraydurasi = dataFilm.map(function(item){
   return item.durasi
})

console.debug(arrayWdurasi)

var arrayGenre = dataFilm.map(function(item){
   return item.genre
})

console.debug(arrayGenre)

var arrayTahun = dataFilm.map(function(item){
   return item.tahun
})

console.debug(arrayTahun)


// SOAL 4
class Animal {
	constructor(band, cold_blooded) {
	this.band = 4
	this.cold_blooded = false
	}
 
}

// soal 4 release 0 
class Animal {
	constructor(name, legs, cold_blooded) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false



// soal 4 release 1 

class Ape extends Animal {
  constructor(name, amount) {
    super(name)
    //override
    this.legs = amount;
  }

  yell() {
    console.log("Auooo")
  }  
}

class Frog extends Animal {
  constructor(name) {
    super(name);
  }

  jump() {
    console.log("hop hop")
  }
}


var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


// soal 5
function Clock({ template }) {
  
  var timer;

  function render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    this.timer = setInterval(this.render.bind(this), 1000);
  }

}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 

