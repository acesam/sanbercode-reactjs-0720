// soal 1
const newFunction = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 

// soal 2

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation}= newObject;

console.log(firstName, lastName, destination, occupation)

// soal3
let combined = [["Will", "Chris", "Sam", "Holly"], ["Gill", "Brian", "Noel", "Maggie"]]
console.log(combined)